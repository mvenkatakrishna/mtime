#include "mtime_mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MTime_MainWindow w;
    w.show();
    return a.exec();
}
