MTime - Date and Time Utility
------------------------------

This is MTime, a simple application to help with routine day-to-day needs for date and time information in a typical office. For example, if you have teams apread over multiple time zones and need to quickly check what the time is in the other zone tomorrow at noon to fix a meeting, you can quickly check in this one. It also provides a simple alarm function, where you can set a short term alarm, say for two minutes, to remind yourself of some important meeting (it is a simple one, has no link to your office/email apps or anything). It also has a calendar view to see the date, weekday, and calendar week number.
