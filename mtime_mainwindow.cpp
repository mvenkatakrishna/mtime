#include "mtime_mainwindow.h"
#include "ui_mtime_mainwindow.h"

#include <QDateTime>
#include <QTimeZone>
#include <QDebug>

MTime_MainWindow::MTime_MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MTime_MainWindow)
{
    ui->setupUi(this);

    m_settings = std::make_unique<QSettings>("mtime_settings.ini", QSettings::IniFormat);
    m_settings->sync();

    m_comboSetupFlag = false;

    m_setupCombos();
    m_updateTimes();

    m_timerRunningFlag = false;
    m_time = QTime(0,0,0);
    connect( &m_timer, &QTimer::timeout, this, &MTime_MainWindow::m_countDown );
    ui->btnStart->setEnabled(true);
    ui->btnPause->setEnabled(false);

    m_ping = std::make_unique<QSound>(":sounds/mtime.wav");
    m_ping->setLoops(QSound::Infinite);

    ui->tabWidget->setCurrentIndex(0);
}

MTime_MainWindow::~MTime_MainWindow()
{
    m_ping->stop();
    delete ui;
}

void MTime_MainWindow::m_updateTimes()
{
    QDateTime srcTime = ui->dateTimeEditSrc->dateTime();
    srcTime.setTimeZone( QTimeZone(ui->comboSource->currentText().toUtf8()) );
    QString format = "dd-MM-yyyy hh:mm:ss";
    ui->label1->setText( srcTime.toTimeZone( QTimeZone(ui->combo1->currentText().toUtf8()) ).toString(format) );
    ui->label2->setText( srcTime.toTimeZone( QTimeZone(ui->combo2->currentText().toUtf8()) ).toString(format) );
    ui->label3->setText( srcTime.toTimeZone( QTimeZone(ui->combo3->currentText().toUtf8()) ).toString(format) );
    ui->label4->setText( srcTime.toTimeZone( QTimeZone(ui->combo4->currentText().toUtf8()) ).toString(format) );
}

void MTime_MainWindow::m_saveState()
{
    //Save app state
    m_settings->setValue( "SourceZone", ui->comboSource->currentIndex() );
    m_settings->setValue( "Zone1", ui->combo1->currentIndex() );
    m_settings->setValue( "Zone2", ui->combo2->currentIndex() );
    m_settings->setValue( "Zone3", ui->combo3->currentIndex() );
    m_settings->setValue( "Zone4", ui->combo4->currentIndex() );
    m_settings->sync();
}

void MTime_MainWindow::on_comboSource_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    if(m_comboSetupFlag)
    {
        m_updateTimes();
        m_saveState();
    }
}

void MTime_MainWindow::on_dateTimeEditSrc_dateTimeChanged(const QDateTime &dateTime)
{
    Q_UNUSED(dateTime)
    m_updateTimes();
    m_saveState();
}

void MTime_MainWindow::on_btnSetCurrTime_clicked()
{
    ui->dateTimeEditSrc->setDateTime( QDateTime::currentDateTime() );
}

void MTime_MainWindow::on_combo1_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    if(m_comboSetupFlag)
    {
        m_updateTimes();
        m_saveState();
    }
}

void MTime_MainWindow::on_combo2_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    if(m_comboSetupFlag)
    {
        m_updateTimes();
        m_saveState();
    }
}

void MTime_MainWindow::on_combo3_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    if(m_comboSetupFlag)
    {
        m_updateTimes();
        m_saveState();
    }
}

void MTime_MainWindow::on_combo4_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    if(m_comboSetupFlag)
    {
        m_updateTimes();
        m_saveState();
    }
}

void MTime_MainWindow::m_setupCombos()
{
    QList<QByteArray> tmp = QTimeZone::availableTimeZoneIds();
    QStringList zoneList;
    for( int i = 0; i < tmp.size(); ++i )
    {
        zoneList.append( QString(tmp[i]) );
    }

    ui->comboSource->addItems(zoneList);
    ui->combo1->addItems(zoneList);
    ui->combo2->addItems(zoneList);
    ui->combo3->addItems(zoneList);
    ui->combo4->addItems(zoneList);

    m_comboSetupFlag = true;

    int idxSrc = m_settings->value("SourceZone").toInt();
    int idx1 = m_settings->value("Zone1").toInt();
    int idx2 = m_settings->value("Zone2").toInt();
    int idx3 = m_settings->value("Zone3").toInt();
    int idx4 = m_settings->value("Zone4").toInt();

    if( (idxSrc >= 0) && (idxSrc < tmp.size()) )
        ui->comboSource->setCurrentIndex(idxSrc);
    else
        ui->comboSource->setCurrentText( QTimeZone::systemTimeZoneId() );

    if( (idx1 >= 0) && (idx1 < tmp.size()) )
        ui->combo1->setCurrentIndex(idx1);
    else
        ui->combo1->setCurrentText( QTimeZone::systemTimeZoneId() );

    if( (idx2 >= 0) && (idx2 < tmp.size()) )
        ui->combo2->setCurrentIndex(idx2);
    else
        ui->combo2->setCurrentText( QTimeZone::systemTimeZoneId() );

    if( (idx3 >= 0) && (idx3 < tmp.size()) )
        ui->combo3->setCurrentIndex(idx3);
    else
        ui->combo3->setCurrentText( QTimeZone::systemTimeZoneId() );

    if( (idx4 >= 0) && (idx4 < tmp.size()) )
        ui->combo4->setCurrentIndex(idx4);
    else
        ui->combo4->setCurrentText( QTimeZone::systemTimeZoneId() );

    ui->dateTimeEditSrc->setDateTime( QDateTime::currentDateTime() );
}

void MTime_MainWindow::on_btnStart_clicked()
{
    m_time = ui->timeEditAlarm->time();
    ui->timeEditAlarm->setEnabled(false);
    ui->btnStart->setEnabled(false);
    ui->btnPause->setEnabled(true);
    ui->labelAlarm->setText( m_time.toString("HH:mm:ss") );
    m_timerRunningFlag = true;
    m_timer.start(1000);
}

void MTime_MainWindow::on_btnPause_clicked()
{
    if(m_timerRunningFlag)
    {
        ui->btnPause->setText("Resume");
        m_timerRunningFlag = false;
    }
    else
    {
        ui->btnPause->setText("Pause");
        m_timerRunningFlag = true;
    }
}

void MTime_MainWindow::on_btnReset_clicked()
{
    m_time = QTime(0,0,0);
    ui->timeEditAlarm->setEnabled(true);
    ui->timeEditAlarm->setTime( m_time );
    ui->labelAlarm->setText( m_time.toString("HH:mm:ss") );
    m_timer.stop();
    m_timerRunningFlag = false;
    ui->btnStart->setEnabled(true);
    ui->btnPause->setEnabled(false);
    m_ping->stop();
}

void MTime_MainWindow::m_countDown()
{
    if(!m_timerRunningFlag)
        return;

    m_time = m_time.addSecs(-1);
    ui->labelAlarm->setText( m_time.toString("HH:mm:ss") );
    if( m_time == QTime(0,0,0) )
    {
        m_ping->play();
        m_timerRunningFlag = false;
    }
}
