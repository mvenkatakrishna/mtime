#ifndef MTIME_MAINWINDOW_H
#define MTIME_MAINWINDOW_H

#include <memory>
#include <QMainWindow>
#include <QSettings>
#include <QTime>
#include <QTimer>
#include <QSound>

QT_BEGIN_NAMESPACE
namespace Ui { class MTime_MainWindow; }
QT_END_NAMESPACE

class MTime_MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MTime_MainWindow(QWidget *parent = nullptr);
    ~MTime_MainWindow();

private slots:
    void on_comboSource_currentIndexChanged(int index);

    void on_dateTimeEditSrc_dateTimeChanged(const QDateTime &dateTime);

    void on_combo1_currentIndexChanged(int index);

    void on_combo2_currentIndexChanged(int index);

    void on_combo3_currentIndexChanged(int index);

    void on_combo4_currentIndexChanged(int index);

    void on_btnStart_clicked();

    void on_btnPause_clicked();

    void on_btnReset_clicked();

    void m_countDown();

    void on_btnSetCurrTime_clicked();

private:
    Ui::MTime_MainWindow *ui;

    std::unique_ptr<QSettings> m_settings;
    bool m_comboSetupFlag = false;

    void m_setupCombos();

    void m_updateTimes();

    void m_saveState();

    bool m_timerRunningFlag = false;
    QTime m_time;
    QTimer m_timer;

    std::unique_ptr<QSound> m_ping;
};
#endif // MTIME_MAINWINDOW_H
